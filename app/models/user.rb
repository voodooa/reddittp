class User < ApplicationRecord
  has_many :posts
  has_many :comments

  VALID_PASSWORD_REGEX = /\A(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,64}\z/
  VALID_NAME_REGEX = /\A[a-zA-Z\s]+\z/

  validates :name, presence: true, length: { in: 3..16 }, format: { with: VALID_NAME_REGEX, message: :name_format }
  validates :password, presence: true, length: { in: 8..64 }, format: { with: VALID_PASSWORD_REGEX, message: :password_format }
  #validates :password_confirmation, presence: true, message: :password_confirmation_presence
  validates :email, presence: true, uniqueness: { message: :email_uniqueness }, format: { with: URI::MailTo::EMAIL_REGEXP, message: :email_format }

# Scopes (exemple si nécessaire)
  # scope :active, -> { where(active: true) }
end
