class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :post
  belongs_to :parent, class_name: 'Comment', optional: true
  has_many :replies, class_name: 'Comment', foreign_key: :parent_id, dependent: :destroy

  validates :content, presence: true, length: { maximum: 500, message: :content_length }

  before_save :set_depth

  def parent_comment_belongs_to_same_post
    if parent && parent.post != post
      errors.add(:parent_id, :same_post_error)
    end
  end

  def comment_depth
    if parent && depth > 3
      errors.add(:base, :depth_error)
    end
  end

  def depth
    read_attribute(:depth) || calculate_depth
  end

  private

  def set_depth
    self.depth = calculate_depth
  end

  def calculate_depth
    parent ? parent.depth + 1 : 0
  end
end
