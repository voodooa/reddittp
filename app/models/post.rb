class Post < ApplicationRecord
  # Associations
  belongs_to :user
  has_many :comments, dependent: :destroy # Supprime les commentaires associés si le post est supprimé

  # Validations
  validates :title, presence: true, length: { maximum: 100 }
  validates :content, presence: true

  # Scopes (exemple si nécessaire)
  # scope :published, -> { where(published: true) }

  # Méthode pour voter positivement
  def upvote
    increment!(:upvotes)
  end

  # Méthode pour voter négativement
  def downvote
    increment!(:downvotes)
  end
end
