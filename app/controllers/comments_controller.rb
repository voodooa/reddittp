class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET /comments
  def index
    @comments = Comment.all
  end

  # GET /comments/1
  def show
    # @comment is already set by the before_action
  end

  # GET /comments/new
  def new
    #@comment = Comment.new(comment_params)
    @comment = Comment.new
    # Rails.logger.info "Received comment params in new comment: #{comment_params.inspect}"
    @comment.post_id = params[:post_id] if params[:post_id]
    @comment.parent_id = params[:parent_id] if params[:parent_id]
  end

  # POST /comments
  def create
    Rails.logger.info "Received comment params in create comment: #{comment_params.inspect}"
    @comment = Comment.new(comment_params)
    @comment.user_id = rand(1..10)  # Attribuer un user_id aléatoire

    if @comment.save
      redirect_to @comment.post, notice: 'Comment was successfully created.'
    else
      @post = Post.find(@comment.post_id)  # Récupérer le post pour le formulaire en cas d'erreur
      render :new
    end
  end

  # GET /comments/1/edit
  def edit
    # @comment is already set by the before_action
  end

  # PATCH/PUT /comments/1
  def update
    if @comment.update(comment_params)
      redirect_to @comment, notice: 'Comment was successfully updated.'
    else
      render :edit
    end
  end

    # DELETE /comments/1
    def destroy
      post = @comment.post  # Conserver le post avant la suppression pour la redirection
      @comment.destroy
      redirect_to post, notice: 'Comment was successfully destroyed.'
    end

      # Use callbacks to share common setup or constraints between actions.
      def set_comment
        @comment = Comment.find(params[:id])
      end

      # Only allow a list of trusted parameters through.
      def comment_params
        params.require(:comment).permit(:content, :post_id, :parent_id, :comment_id)
      end
  end

