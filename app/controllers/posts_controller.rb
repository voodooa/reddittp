class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  # GET /posts
  def index
    @posts = Post.all
  end

  # GET /posts/1

    def show
      @post = Post.find(params[:id])
      @comment = Comment.new
    end


  # GET /posts/new
  def new
    @post = Post.new
  end

  # POST /posts
  def create
    # Assignation d'un user_id aléatoire entre 1 et 10 à new avant de passer à post_params
    @post = Post.new(post_params.merge(user_id: rand(1..10)))

    if @post.save
      redirect_to @post, notice: 'Post was successfully created.'
    else
      # Affiche les messages d'erreur
      @post.errors.full_messages.each { |message| puts message }

      render :new
    end
  end


  # GET /posts/1/edit
  def edit
    # @post is already set by the before_action
  end

  # PATCH/PUT /posts/1
  def update
    if @post.update(post_params)
      redirect_to @post, notice: 'Post was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /posts/1
  def destroy
    @post.destroy
    redirect_to posts_url, notice: 'Post was successfully destroyed.'
  end

  def upvote
    @post = Post.find(params[:id])
    @post.upvote
    redirect_to @post, notice: 'Merci pour votre vote positif!'
  end

  def downvote
    @post = Post.find(params[:id])
    @post.downvote
    redirect_to @post, notice: 'Vote négatif enregistré.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id])
  end



  # Only allow a list of trusted parameters through.
  def post_params
    params.require(:post).permit(:title, :content, :upvotes, :downvotes, :user_id)
  end
end