# app/helpers/comments_helper.rb
module CommentsHelper
  def render_comments(comments, depth = 0)
    safe_join(comments.map do |comment|
      render(partial: 'comments/comment', locals: { comment: comment, depth: depth }) +
        render_comments(comment.children, depth + 1) unless comment.children.empty?
    end)
  end
end
