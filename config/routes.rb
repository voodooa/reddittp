Rails.application.routes.draw do
  # Routes CRUD simplifiées
  resources :comments
  resources :posts do
    member do
      post 'upvote'
      post 'downvote'
    end
  end
  resources :users

  # Route de vérification de santé de l'application
  get "up" => "rails/health#show", as: :rails_health_check

  # Définition de la route racine, vous pouvez choisir la page appropriée comme racine
  root "home#index"
end
