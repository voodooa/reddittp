I18n.enforce_available_locales = true
I18n.available_locales = [:fr, :en]
I18n.default_locale= :fr

I18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
I18n.backend.load_translations
# Compare this snippet from config/initializers/new_framework_defaults.rb:
#
